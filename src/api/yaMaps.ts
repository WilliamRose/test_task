import { Coords } from '@my-types/ym';

const API_KEY = process.env.API_KEY;

export const loadYMScript = (onLoad: () => void): void => {
  const script = document.createElement('script');
  script.src = `https://api-maps.yandex.ru/2.1/?apikey=${API_KEY}&lang=ru_RU`;
  script.async = true;
  script.onload = onLoad;
  document.body.appendChild(script);
};

export const createYM = (mapContainerId: string, center: Coords) => {
  return new window.ymaps.Map(mapContainerId, {
    center,
    zoom: 15,
  });
};
