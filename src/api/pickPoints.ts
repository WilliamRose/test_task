import { PickPointsServerData, PickPointsData } from '@my-types/api';

const host = process.env.NODE_ENV === 'production' ? 'https://express-shina.ru' : '';

export const getPickPoints = async (): Promise<PickPointsData> => {
  let data: PickPointsData = {
    pickPoints: [],
    success: false,
    error: '',
  };

  await fetch(`${host}/vacancy/geo-state`).then(async (res: Response) => {
    if (res.ok) {
      await res.json().then((response: PickPointsServerData) => {
        const { pickPoints } = response;
        if (pickPoints) {
          data.pickPoints = pickPoints;
          data.success = true;
        } else {
          data.error = 'Server response has no pickPoints.';
        }
      });
    } else {
      data.success = false;
      data.error = `Server responded with ${res.status}.`;
    }
  }).catch((e) => {
    data.error = e.message;
  });

  return data;
};
