import React, { Suspense } from 'react';

import s from './ErrorBoundary.module.scss';

type State = {
  error: string;
  hasError: boolean;
};

type Props = {
  children: React.ReactNode;
};

class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false, error: '' };
  }

  static getDerivedStateFromError(error: Error) {
    return { hasError: true, error: error.message };
  }

  componentDidCatch = (error: Error) => {
    this.setState({ hasError: true, error: error.message });
  };

  render() {
    const { children } = this.props;
    const { hasError, error } = this.state;
    if (hasError) {
      return (
        <div className={s.error_wrapper}>
          <span className={s.oops}>
            Ooops...
          </span>
          <span className={s.error_message}>
            {error}
          </span>
        </div>
      );
    }
    return children;
  }
}

export default ErrorBoundary;
