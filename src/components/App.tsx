import React, { FC, useEffect, useState } from 'react';

import { PickPoint } from '@my-types/api';

import { getPickPoints } from '@api/pickPoints';
import { loadYMScript } from '@api/yaMaps';

import { Loader } from './Loader/Loader';
import { PickupBlock } from './PickupBlock/PickupBlock';

import s from './App.module.scss';

declare global {
  interface Window {
    ymaps: any;
  }
};

const App: FC = (): JSX.Element => {
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [mapIsReady, setMapIsReady] = useState(false);
  const [pickPoints, setPickPoints] = useState([] as PickPoint[]);
  const [currentPointIndex, setCurrentPointIndex] = useState(0);
  const [needToCorrectPoint, setNeedToCorrectPoint] = useState(true);
  const [pointChangeBlocked, setPointChangeBlocked] = useState(false);

  const setCurrentPointIndexWrapped = (id: number) => {
    if (!pointChangeBlocked) {
      setCurrentPointIndex(id);
      setNeedToCorrectPoint(false);
      requestAnimationFrame(() => {
        setNeedToCorrectPoint(true);
      });
    }
  }

  useEffect(() => {
    setLoading(true);
    getPickPoints().then((response) => {
      setLoading(false);
      const { success, pickPoints, error } = response;
      if (success) {
        setPickPoints(pickPoints);
      } else {
        setError(error);
      }
    });
    loadYMScript(() => {
      window.ymaps.ready(() => {
        setMapIsReady(true);
      });
    });
  }, []);

  useEffect(() => {
    if (error) {
      throw new Error(error);
    }
  }, [error]);

  return (
    <div className={s.app}>
      {loading ? (
        <Loader />
      ) : (
        <PickupBlock
          mapIsReady={mapIsReady}
          pickPoints={pickPoints}
          currentPointIndex={currentPointIndex}
          needToCorrectPoint={needToCorrectPoint}
          setCurrentPointIndex={setCurrentPointIndexWrapped}
          setPointChangeBlocked={setPointChangeBlocked}
        />
      )}
    </div>
  );
};

export default App;
