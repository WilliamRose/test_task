import React, { FC } from 'react';

import s from './Loader.module.scss';

export const Loader: FC = () => {
  return (
    <div className={s.loader_wrapper}>
      <div className={s.loader} />
      <div className={s.loader_title}>
        Загрузка
      </div>
    </div>
  );
}