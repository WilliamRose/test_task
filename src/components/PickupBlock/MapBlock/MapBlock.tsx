import React, { FC, useEffect, useRef } from 'react';

import { PickPoint } from '@my-types/api';
import { Coords } from '@my-types/ym';

import { createYM } from '@api/yaMaps';

import s from './MapBlock.module.scss';

type Props = {
  mapIsReady: boolean;
  pickPoints: PickPoint[];
  currentPointIndex: number;
  needToCorrectPoint: boolean;
  setCurrentPointIndex: (pointIndex: number) => void;
  setPointChangeBlocked: (isBlocked: boolean) => void;
};

export const MapBlock: FC<Props> = ({
  pickPoints,
  mapIsReady,
  currentPointIndex,
  needToCorrectPoint,
  setCurrentPointIndex,
  setPointChangeBlocked,
}): JSX.Element => {
  const mapContainerId = 'map-container';
  const ymap = useRef(null);

  const currentPointIndexObj = pickPoints[currentPointIndex];

  useEffect(() => {
    if (mapIsReady && needToCorrectPoint) {
      const { latitude, longitude } = currentPointIndexObj;
      const center: Coords = [latitude, longitude];
      if (ymap.current === null) {
        ymap.current = createYM(mapContainerId, center);
        ymap.current.options.set('maxAnimationZoomDifference', Infinity);
        pickPoints.forEach((point, i) => {
          const { latitude, longitude, address } = point;
          const coordinates: Coords = [latitude, longitude];
          const placemark = new window.ymaps.Placemark(coordinates, {
            iconCaption: address,
          }, {
            preset: 'islands#redDotIcon'
          });
          placemark.events.add('click', () => {
            setCurrentPointIndex(i);
          });
          ymap.current.geoObjects.add(placemark);
        });
      } else {
        setPointChangeBlocked(true);
        ymap.current.panTo(center).then(() => {
          setPointChangeBlocked(false);
          ymap.current.setZoom(15);
        });
      }
    }
  }, [mapIsReady, needToCorrectPoint, currentPointIndexObj?.latitude, currentPointIndexObj?.longitude]);

  return (
    <div id={mapContainerId} className={s.map} />
  );
};
