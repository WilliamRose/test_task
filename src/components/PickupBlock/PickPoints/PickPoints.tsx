import React, { FC } from 'react';

import { PickPoint } from '@my-types/api';

import { OnePickPointCard } from './OnePickPointCard/OnePickPointCard';

import s from './PickPoints.module.scss';

type Props = {
  pickPoints: PickPoint[];
  currentPointIndex: number;
  setCurrentPointIndex: (pointIndex: number) => void;
};

export const PickPoints: FC<Props> = ({
  pickPoints,
  currentPointIndex,
  setCurrentPointIndex,
}): JSX.Element => (
  <div className={s.pick_points}>
    {pickPoints.map((pickPoint, i) => {
      const onClickHandler = () => setCurrentPointIndex(i);
      const isChoosed = currentPointIndex === i;

      return (
        <OnePickPointCard
          key={pickPoint.address}
          isChoosed={isChoosed}
          pickPoint={pickPoint}
          onClickHandler={onClickHandler}
        />
      );
    })}
  </div>
);
