import React, { FC } from 'react';
import classNames from 'classnames';

import { PickPoint } from '@my-types/api';

import s from './OnePickPointCard.module.scss';

type Props = {
  isChoosed: boolean;
  pickPoint: PickPoint;
  onClickHandler: () => void;
};

export const OnePickPointCard: FC<Props> = ({
  isChoosed,
  pickPoint: {
    address,
    budgets,
  },
  onClickHandler,
}): JSX.Element => {
  const className = classNames(s.card, isChoosed && s['card--selected']);

  return (
    <button  className={className} onClick={onClickHandler}>
      {address}
      <div className={s.budgets_place}>
        {budgets.map(((text) => (
          <div key={text} className={s.badge}>
            {text}
          </div>
        )))}
      </div>
    </button>
  );
};
