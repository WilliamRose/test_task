import React, { FC } from 'react';

import { PickPoint } from '@my-types/api';

import { PickPoints } from './PickPoints/PickPoints';
import { MapBlock } from './MapBlock/MapBlock';

import s from './PickupBlock.module.scss';

type Props = {
  mapIsReady: boolean;
  pickPoints: PickPoint[];
  currentPointIndex: number;
  needToCorrectPoint: boolean;
  setCurrentPointIndex: (pointIndex: number) => void;
  setPointChangeBlocked: (isBlocked: boolean) => void;
};

export const PickupBlock: FC<Props> = ({
  mapIsReady,
  pickPoints,
  currentPointIndex,
  needToCorrectPoint,
  setCurrentPointIndex,
  setPointChangeBlocked,
}): JSX.Element => {

  return (
    <div className={s.block_wrapper}>
      <PickPoints
        pickPoints={pickPoints}
        currentPointIndex={currentPointIndex}
        setCurrentPointIndex={setCurrentPointIndex}
      />
      <MapBlock
        mapIsReady={mapIsReady}
        pickPoints={pickPoints}
        currentPointIndex={currentPointIndex}
        needToCorrectPoint={needToCorrectPoint}
        setCurrentPointIndex={setCurrentPointIndex}
        setPointChangeBlocked={setPointChangeBlocked}
      />
    </div>
  );
};
