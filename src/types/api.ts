export enum Budget {
  'Самовывоз',
  'Доставка',
  'Шоурум',
  'Примерочная'
};

export type PickPoint = {
  address: string;
  budgets: Budget[];
  latitude: number;
  longitude: number;
};

export type PickPointsServerData = {
  pickPoints: PickPoint[];
}

export type PickPointsData = {
  pickPoints?: PickPoint[];
  success?: boolean;
  error?: string;
};
